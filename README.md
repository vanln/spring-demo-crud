# Spring-demo-CRUD


## Description
    This program provides basic CRUD functions.
    Depending on the user, the program will provide different functions based on ROLE

## Prepare the environment
    MySQL: Version 8.0.33
    Java:  version 17.0.7

## Install project

    1. Clone project from gitlab
            Step 1: From a folder, right click and open Git Bash

            Step 2: Connect to gitlab account with two command lines
                git config --global user.name "Your GitLab Username"
                git config --global user.email "your-email@example.com"

            Step 3: Clone project
                In Git Bash, uses this command to clone project  from gitlab
                git clone [repository_url]

    2. Import code in the IDE
           Open the folder you just cloned from gitlab in the IDE

    3. Prepare database
           Open file application.properties and SpringSecurityApplication.java
                Check URL, Username, Password, Name of Database and Port
                example:
                    spring.datasource.url=jdbc:mysql://localhost:<port>/<name of database>
                    spring.datasource.username=<username>
                    spring.datasource.password=<password>
                    spring.datasource.driverClassName=com.mysql.cj.jdbc.Driver
    4. Run Application
           Run this command in terminal: mvnw spring-boot:run

## Use program
    Login with account in database
        - Admin: van12345/111115
        - User: van/111115
        
    The option on Roles will be taken to different homepages
        - Admin: admin has the function of list view, search, update, delete, create new and logout.
        _ User: user has only list view, search and logout function.

