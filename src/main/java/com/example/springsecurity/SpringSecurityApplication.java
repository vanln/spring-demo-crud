package com.example.springsecurity;

import jakarta.persistence.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@SpringBootApplication
public class SpringSecurityApplication {
    
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/?serverTimezone=UTC&useSSL=false";
  
    static final String USER = "root";
    static final String PASS = "111115";
    public static void main(String[] args) throws ClassNotFoundException {
        Class.forName(JDBC_DRIVER);
        try (Connection connection = DriverManager.getConnection(DB_URL, USER, PASS)) {
           
            Statement stmt;
            stmt = connection.createStatement();
            String sql = "CREATE DATABASE IF NOT EXISTS springdemo";
            stmt.executeUpdate(sql);
    
        } catch (SQLException e) {
            e.printStackTrace();
        }
        SpringApplication.run(SpringSecurityApplication.class, args);
    }

    }


