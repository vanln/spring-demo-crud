package com.example.springsecurity.controller;

import com.example.springsecurity.service.CustomerService;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.Authenticator;
import java.util.Collection;

@Controller
@SpringBootApplication
public class LoginController {

    private CustomerService customerService;

    public LoginController (CustomerService theCustomerService){
        customerService= theCustomerService;
    }
    @GetMapping("/homePage")
    public String homePage(Authentication authentication){
        if (authentication != null) {
            Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
            if (authorities.stream().anyMatch(auth -> auth.getAuthority().equals("ROLE_ADMIN"))) {
                return "redirect:/homeadmin";
            } else if (authorities.stream().anyMatch(auth -> auth.getAuthority().equals("ROLE_USER"))) {
                return "redirect:/homeuser";
            }
        }


        return "redirect:/home";

    }

    @GetMapping("/homeuser")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public String Homeuser(Model theModel,Authentication authentication){
        theModel.addAttribute("name",authentication.getName());
        theModel.addAttribute("role",authentication.getAuthorities());
        theModel.addAttribute("customers",customerService.findAll());
        return "homeuser";
    }

    @GetMapping("/homeadmin")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public  String Homeadmin( Model theModel,Authentication authentication){

        theModel.addAttribute("name",authentication.getName());
        theModel.addAttribute("role",authentication.getAuthorities());
        theModel.addAttribute("customers",customerService.findAll());
        return "homeadmin";
    }



}
