package com.example.springsecurity.controller;

import com.example.springsecurity.entity.Customer;
import com.example.springsecurity.service.CustomerService;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@SpringBootApplication
public class SwitchPage {

    @GetMapping("/switch")
    public String Switch (@RequestParam("page") String page ){


        return page;
    }

}
