package com.example.springsecurity.controller;

import com.example.springsecurity.entity.Customer;
import com.example.springsecurity.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class CustomerController {
    private CustomerService customerService;

    public CustomerController( CustomerService theCustomerService){
        customerService=theCustomerService;
    }



    @GetMapping("customer/search")
    public String search(@RequestParam("input") String input ,@RequestParam("type") String type ,Authentication authentication, Model theModel) {
        theModel.addAttribute("name",authentication.getName());
        theModel.addAttribute("role",authentication.getAuthorities());
        theModel.addAttribute("customers",customerService.findByName(input));
        if(type.equals("admin")){
        return "homeadmin";
        }
        else{
            return "homeuser";
        }
    }
    @PostMapping("customer/add")
    public String add (@RequestParam("name") String name,@RequestParam("email") String email ) {

       Customer customer=new Customer();
       customer.setEmail(email);
       customer.setName(name);
       customerService.saveCustomer(customer);
        return "addcomplete";
    }

    @GetMapping("customer/delete")
    public String delete (@RequestParam("id") long id ) {
        System.out.println(id);

        customerService.deleteCustomer(id);

        return "deletecomplete";
    }

    @GetMapping("/gotoupdatecustomer")
    public String GotoupdateCustomer(@RequestParam("id") long id, Model theModel){
        Customer customer= new Customer();
        customer = customerService.findById( id );

        theModel.addAttribute("customer",customer);
        return "updatecustomer";
    }
    @PostMapping("/customer/update")
    public String UpdateCustomer(@RequestParam("id") long id,@RequestParam("name") String name,@RequestParam("email") String email){
        Customer customer= new Customer();
        customer.setId(id);
        customer.setEmail(email);
        customer.setName(name);
        customerService.saveCustomer(customer);
        return "addcomplete";
    }


}
