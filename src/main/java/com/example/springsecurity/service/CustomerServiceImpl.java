package com.example.springsecurity.service;

import com.example.springsecurity.entity.Customer;
import com.example.springsecurity.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImpl implements CustomerService {
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository theCustomerRepository){
        customerRepository = theCustomerRepository;
    }

    @Override
    public List<Customer> findAll(){
        return customerRepository.findAllByOrderByIdAsc();
    }

    @Override
    public List<Customer> findByName(String name){return customerRepository.findByNameContaining(name);}

     @Override
    public Object saveCustomer (Customer customer){ return customerRepository.save (customer);}

    @Override
    public void deleteCustomer (long id){
      customerRepository.deleteById(id);
    }

    @Override
    public Customer findById (long id){ return customerRepository.findById(id); }
}
