package com.example.springsecurity.service;

import com.example.springsecurity.entity.Customer;

import java.util.List;

public interface CustomerService {
    public List<Customer> findAll();
    public List<Customer> findByName(String name);
    public Object saveCustomer(Customer customer);
    public void deleteCustomer(long id);
    public Customer findById (long id);
}
