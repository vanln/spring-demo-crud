package com.example.springsecurity.service;

import com.example.springsecurity.entity.Customer;
import com.example.springsecurity.entity.UserInfo;
import com.example.springsecurity.repository.CustomerRepository;
import com.example.springsecurity.repository.UserInfoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

//Em đã tìm hiểu cách để Spring tự động tạo bảng thông qua Entity và thông qua chạy file schema.sql và data.sql
// Code này hiện tại em đang dùng cách chạy file sql
// Đoạn code này là em dùng đẻ test cách dùng entity chư không tiên quan đến chạy chương trình hiện tại ạ.





@Service
public class ImportDataService {
    private  CustomerRepository customerRepository;
    private UserInfoRepository userInfoRepository;

    public ImportDataService(CustomerRepository theCustomerRepository, UserInfoRepository theUserInfoRepository) {
        this.customerRepository = theCustomerRepository;
        this.userInfoRepository = theUserInfoRepository;
    }
        //Customer
        Customer customer1= new Customer(1L,"Levan","levan@gmai.com");
        Customer customer2= new Customer(2L,"Dinhtien","dinhtien@gmai.com");  
        Customer customer3= new Customer(3L,"Danghoa","danghoa@gmai.com");
        Customer customer4= new Customer(4L,"Vantung","vantung@gmai.com");
        public void addCustomer() {
            customerRepository.save(customer1);
            customerRepository.save(customer2);
            customerRepository.save(customer3);
            customerRepository.save(customer4);
        }

        //UserInfo
    UserInfo userInfo1= new UserInfo();
    UserInfo userInfo2= new UserInfo();

//    public void addUserInfo() {
//
//        UserInfo save = UserInfoRepository.save(userInfo1);
//        UserInfoRepository.save(userInfo2);
//
//    }
    }


