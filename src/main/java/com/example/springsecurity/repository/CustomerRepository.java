package com.example.springsecurity.repository;

import com.example.springsecurity.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.yaml.snakeyaml.events.Event;
import org.yaml.snakeyaml.tokens.Token;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    public List<Customer> findAllByOrderByIdAsc();
    public List<Customer> findByNameContaining(String name);
    public Customer findById(long id);

}
