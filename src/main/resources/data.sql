-- TRUNCATE `customer`
INSERT INTO `customer` VALUES (1,'Levan','levan@gmail.com'),
                              (2,'Dinhtien','dinhtien@hmail.com'),
                              (3,'Nguyenyen','nguyenyen@gmail.com'),
                              (4,'Vantung','vantung@gmail.com');

-- TRUNCATE `user_info`
INSERT INTO `user_info` VALUES (1,'van@gmail.com','van','$2y$10$MTc8IiDVGIFPgMek4oQWJel6peDs08bAzJvQuGiZxq0R4Zg1tODbW','ROLE_USER'),
                               (4,'van12345@gmail.com','van12345','$2a$10$hKkrJ0a6zhruSKpwuf.HnussGui6pFiz8TB63rf9c.zyWxSDUUKmW','ROLE_ADMIN');