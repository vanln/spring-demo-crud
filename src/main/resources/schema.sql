-- DROP TABLE `customer`
CREATE TABLE `customer` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `name` varchar(45) NOT NULL,
                            `email` varchar(45) NOT NULL,
                            PRIMARY KEY (`id`)
);

-- DROP TABLE `customer`
CREATE TABLE `user_info` (
                             `id` int NOT NULL AUTO_INCREMENT,
                             `email` varchar(255) DEFAULT NULL,
                             `name` varchar(255) DEFAULT NULL,
                             `password` varchar(255) DEFAULT NULL,
                             `roles` varchar(255) DEFAULT NULL,
                             PRIMARY KEY (`id`)
);

